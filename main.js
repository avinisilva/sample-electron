const { app, BrowserWindow, ipcMain, Tray, Menu } = require('electron')

let tray = null
app.on('ready', () => {
  tray = new Tray(__dirname + '/app/img/icon-tray.png');
  
  let trayMenu = Menu.buildFromTemplate([
      { label: 'Menu01', type: 'radio' }
  ])
  
  tray.setContextMenu(trayMenu)
    
  let window = new BrowserWindow({
      width: 600,
      height: 400
  })

  window.loadURL(`file://${__dirname}/app/index.html`)
})

app.on('window-all-closed', () => {
    app.quit()
})

let aboutWindow = null
ipcMain.on('open-window-about', () => {

    if(aboutWindow === null) {
        aboutWindow = new BrowserWindow({
            width: 300,
            height: 300,
            alwaysOnTop: true,
            frame: false
        })

        aboutWindow.on('closed', () => {
            aboutWindow = null
        })
    }

    aboutWindow.loadURL(`file://${__dirname}/app/about.html`)
})

ipcMain.on('close-window-about', () => {
    aboutWindow.close()
})