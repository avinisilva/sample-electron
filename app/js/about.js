const { ipcRenderer, shell } = require('electron') 
const process = require('process')

const closeLink = document.querySelector('#close-link')
const contactLink = document.querySelector('#contact-link')
const electronVersion =  document.querySelector('#electron-version')

window.onload = function() {
    electronVersion.textContent = process.versions.electron
}

closeLink.addEventListener('click', (event) => {
    event.preventDefault()
    ipcRenderer.send('close-window-about')
})

contactLink.addEventListener('click', (event) => {
    event.preventDefault()
    shell.openExternal('https://github.com/avinicius-adorno')
})