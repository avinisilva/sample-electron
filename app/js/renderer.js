const { ipcRenderer } = require('electron') 

const aboutLink = document.querySelector('#about-link')

aboutLink.addEventListener('click', (event) => {
    event.preventDefault()
    ipcRenderer.send('open-window-about')
    new Notification('Test', { body: 'Notification'})
})